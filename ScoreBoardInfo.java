import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import net.dsuke.api.ScoreBoardAPI;
import net.dsuke.support.DsukePlayer;



public class ScoreBoardInfo {
	private Player player;
	private Scoreboard scoreboard;//スコアボード
	private Objective objective;
	private String[] words;
	private DsukePlayer playerInfo;
	private String title;
	/**
	 * 新規作成
	 * @param info
	 */
	public ScoreBoardInfo(DsukePlayer info){
		this.playerInfo = info;
		this.player = info.getBukkitPlayer();
		Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		player.setScoreboard(scoreboard);
		this.scoreboard = scoreboard;
		this.syncMainScoreboard();

		Objective obj = this.scoreboard.getObjective("guns");
		if ( obj == null ) {// 取得できなかったら新規作成する
			obj = this.scoreboard.registerNewObjective("gun", "dummy");
		}
		this.objective=obj;
		words = new String[15];
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
	}
	public Scoreboard getScoreboard(){
		return scoreboard;
	}
	public void resetWords(){
		words = new String[15];
	}

	/**
	 * 0 <= number <= 14
	 * @param word
	 * @param number
	 */
	public void setWord(String word,int number){
		if(0<=number&&number<=14){
			for(int i = words.length-1; i>=0;i--){
				//重複チェック
				if(words[i]!=null&&i!=number&&words[i].equals(word)){
					word+=getColor(number);
					break;
				}
			}
			words[number]=word;
		}
	}
	private ChatColor getColor(int number){
		int i = 0;
		for(ChatColor color:ChatColor.values()){
			if(i==number){
				return color;
			}
			i++;
		}
		return ChatColor.RESET;
	}
	/**
	 * Listからset
	 * @param arrays
	 */
	public void setWords(List<String> arrays){
		int score = arrays.size();
		int line=score;
	    int max = 15;
	    for(int point=0;1<=score;){
			if(max<=point){
				break;
			}
			this.setWord(arrays.get(line-score), score);
			
			point++;
		    score--;
	    }
	}

	public void setTitle(String title){
		this.title=title;
	}
	/**
	 * 更新。
	 */
	public void updateScoreBoard(){
		objective.setDisplayName(title);
		for(int i = words.length-1; i>=0;i--){
			int score=i+1;
			String word = words[i];
			if(word!=null)objective.getScore(word).setScore(score);
		}
		for(String string:scoreboard.getEntries()){
			boolean b = true;
			for(int i = words.length-1; i>=0;i--){
				//含まれていなかった場合除去
				if(words[i]!=null&&words[i].equals(string)){
					b=false;
					break;
				}
			}
			if(b)scoreboard.resetScores(string);
		}
	}

	
	/**
	 * メインと同期させる。
	 */
	public void syncMainScoreboard(){
		ScoreBoardAPI.syncMainScoreboard(scoreboard);
	}
}
