import java.util.List;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import net.dsuke.DaisukeGunOnline;
import net.dsuke.support.DsukePlayer;
 
public class ScoreBoardAPI {

	public static ChatColor defaultColor = ChatColor.WHITE;

	public static ChatColor getDefaultColor() {
		return defaultColor;
	}

	public static void setDefaultColor(ChatColor defaultColor) {
		ScoreBoardAPI.defaultColor = defaultColor;
	}

	public static String[] colors = new String[]{"aqua","black","blue","dark_aqua","dark_blue","dark_gray","dark_green","dark_purple"
			,"dark_red","gold","gray","green","light_purple","red","white","yellow"};
 
	public static void leavePlayerTeam(DsukePlayer leave){
		Scoreboard main = getMainScoreboard();
		leavePlayerTeam(main,leave.getBukkitPlayer());
		List<DsukePlayer> players = DaisukeAPI.getOnlinePlayerInfo();
		for(int i = players.size() - 1; i >= 0; i--){
			Scoreboard  scoreboard = players.get(i).getScoreBoardInfo().getScoreboard();
			leavePlayerTeam(scoreboard,leave.getBukkitPlayer());
		}
	}
	@SuppressWarnings("deprecation")
	private static void leavePlayerTeam(Scoreboard scoreboard,Player player) {
		Team team = getPlayerTeam(scoreboard,player);
		if (team != null){
			team.removePlayer(player);
		}
	}
	@SuppressWarnings("deprecation")
	private static Team getPlayerTeam(Scoreboard scoreboard,Player player) {
		Set<Team> teams = scoreboard.getTeams();
		for (Team team : teams) {
			if(team!=null){
				for (OfflinePlayer p : team.getPlayers()) {
					if(p.isOnline()){
						if (p.getName().equalsIgnoreCase(player.getName())) {
							return team;
						}
					}else{
						team.removePlayer(p);
					}
				}
			}
		}
		return null;
	}
	/**
	 * プレイヤーをチームに追加する(ゲーム用)
	 * @param add
	 * @param color
	 */
	public static void addPlayerTeam(DsukePlayer add, String color){
		leavePlayerTeam(add);
		Scoreboard main = getMainScoreboard();
		addPlayerTeam(main,add.getBukkitPlayer(),color);
		List<DsukePlayer> players = DaisukeAPI.getOnlinePlayerInfo();
		for(int i = players.size() - 1; i >= 0; i--){
			Scoreboard  scoreboard = players.get(i).getScoreBoardInfo().getScoreboard();
			addPlayerTeam(scoreboard,add.getBukkitPlayer(),color);
		}
	}
	/**
	 * 個人のスコアボードにチームを作って返すメソッド
	 * @param scoreboard - 追加するスコアボード
	 * @param player - 追加するプレイヤー
	 * @param color - 入れる色
	 */
	@SuppressWarnings("deprecation")
	private static Team addPlayerTeam(Scoreboard scoreboard,Player player,String color){
		Team team = addTeam(scoreboard,color);
		team.addPlayer(player);
		//フレンドリーファイアを有効に
		team.setAllowFriendlyFire(false);
		team.setCanSeeFriendlyInvisibles(false);
		//違うチームからタグが見えないようにする。
		team.setNameTagVisibility(NameTagVisibility.HIDE_FOR_OTHER_TEAMS);
		return team;
	}
	/**
	 * 個人のスコアボードにカラーチームを追加して返す。
	 */
	private static Team addTeam(Scoreboard scoreboard,String colorName){
		return addTeam(scoreboard,colorName,ColorReplace(colorName)+colorName+ChatColor.RESET.toString(),ColorReplace(colorName).toString(),ChatColor.RESET.toString());
	}
	/**
	 * 個人のスコアボード(scoreboard)にチームを作って返す。
	 * @param scoreboard
	 * @return
	 */
	private static Team addTeam(Scoreboard scoreboard,String teamName,String displayName,String prefix,String suffix) {
		Team team = scoreboard.getTeam(teamName);
		if(team == null){
			team = scoreboard.registerNewTeam(teamName);
			team.setDisplayName(displayName);
			team.setPrefix(prefix);
			team.setSuffix(suffix);
			team.setAllowFriendlyFire(true);
		}
		return team;
	}
	/**
	 * 文字列から色を返す。
	 * @param color
	 * @return
	 */
	public static ChatColor ColorReplace(String color) {
		boolean check = false;
		for(String name:colors){
			if(name.equals(color)){
				check = true;
				break;
			}
		}
		if(check){
			return ChatColor.valueOf(color.toUpperCase());
		}else{
			return getDefaultColor();
		}
	}
	/**
	 * メインスコアボードを返す。
	 * @return
	 */
    public static Scoreboard getMainScoreboard() {
        return DaisukeGunOnline.instance.getServer().getScoreboardManager().getMainScoreboard();
    }
 
	/**
	 * メインと同期させる。
	 */
	@SuppressWarnings("deprecation")
	public static void syncMainScoreboard(Scoreboard scoreboard){
		Scoreboard main = ScoreBoardAPI.getMainScoreboard();
		for(Team mteam:main.getTeams()){
			for(OfflinePlayer mplayer:mteam.getPlayers()){
				if(mplayer.isOnline()){
					Player add = (Player)mplayer;
					ScoreBoardAPI.addPlayerTeam(scoreboard,add,mteam.getName());
				}
			}
		}
	}

 
}
